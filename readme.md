# UserCSS/UserScript collection

Click-to-install links for UserCSS stylesheets require [Stylus](https://add0n.com/stylus.html) or similar.
For UserScripts, [TamperMonkey](https://tampermonkey.net) or similar.

### e621 legibility

[Click to install](https://gitlab.com/F3SW/usercss-collection/-/raw/master/usercss/e621_legibility.user.css)
for [e621](https://e621.net) (NSFW!) and [e926](https://e926.net) (same site but with explicit images hidden).

Changes some text colours for improved legibility. Note this assumes you're using the default "Hexagon" theme.

### E-Hentai zoom

[Click to install](https://gitlab.com/F3SW/usercss-collection/-/raw/master/usercss/ehentai_zoom.user.css)
for [E-Hentai](https://e-hentai.org/) (NSFW!).

Shrinks images so nothing is cut off by the edge of the window.

### Fur Affinity content focus

[Click to install](https://gitlab.com/F3SW/usercss-collection/-/raw/master/usercss/fa_focus.user.css)
for [Fur Affinity](https://furaffinity.net).

Shrinks the header on user pages and stops overlays from covering posts.

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received [a copy of the GNU General Public License](license.md) along with this program.  If not, see [gnu.org/licenses](https://www.gnu.org/licenses).
